let filter =(elements , cb)=>
{   
    let arr=[]
    if(Array.isArray(elements)==false)
    {
        return 0;
    }
    for(let index =0; index<elements.length; index++)
    { 
       if(cb(elements[index],index,elements)===true)
       {
       arr.push(elements[index]);
       }

    }
    return arr;
}
module.exports = filter;