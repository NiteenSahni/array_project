const map = (elements, cb) => {
    let arr = []
    
    if (Array.isArray(elements) == false) {
        return 0
    }
    for (let index = 0; index < elements.length; index++) {
        
        arr.push(cb(elements[index],index,elements))
    }
    return arr;
}
module.exports = map;