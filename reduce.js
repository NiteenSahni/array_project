let reduce = (elements, cb, startingValue) =>
{   

    let acc;
    if(elements.length==0&&startingValue==undefined)
    {
        return undefined;
    }
    if(elements.length<1)
    {
        return startingValue
    }
   
    if(Array.isArray(elements)==false)
    {
        return 0
    }
    for(let index =0; index<elements.length;index++)
    {   if(index==0&&startingValue!==undefined)
        {
             acc = cb(startingValue,elements[index],index,elements)
            
        }
        else if(index==0&&startingValue==undefined)
        {
            acc = elements[0]
        }
       
        if(index>0&&index<elements.length-1){
            acc = cb(acc, elements[index],index,elements)
        }
        if(index==elements.length-1)
        {   
            
            return cb(acc,elements[index],index,elements)
        }
   

    }
}
module.exports = reduce

