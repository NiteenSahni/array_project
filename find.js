let find = (elements, cb) => {
    let def;
    if (Array.isArray(elements) == false) {
        return 0
    }
    for (let index = 0; index < elements.length; index++) {
        def = cb(elements[index])
        if (def !== undefined) {
            return def
        }
        if (index == elements.length - 1) {
            return undefined
        }
    }
}
module.exports = find;