
let flatten = (array, depths)=>
{   
     let depth = depths
    if(depth===undefined)
    {
         depth = 1;
    }
    
    let newArr = []
    for(let index = 0; index<array.length; index++)
    {
      
         if(Array.isArray(array[index])==true&&depth!==0)
        {   
            
            newArr = newArr.concat(flatten(array[index],depth-1))
            
        }

       else if(Array.isArray(array[index])==false&&array[index]!==undefined)
        { 
            newArr.push(array[index])
        }
        else if(Array.isArray(array[index])==true&&depth==0)
        {   
            newArr.push(array[index])
            
        }
        else{
            continue
        }
        
    }
    return newArr
}

module.exports = flatten
